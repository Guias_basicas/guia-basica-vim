<h1 align="center"> Guia basica Vim </h1>

<br>

<div align="center"><img src="vim-header.png" width="450px"></div>

# ¿Que es el editor vim?

VIM es un editor de texto incorporado en todos los sistemas UNIX. Proviene de otro editor, VI, aunque con el paso del tiempo se han ido implementando mejoras.

**Ventajas y desventajas de este editor**

A diferencia de otros editores de texto, este esta pensado para ser utilizado en la terminal del ordenador, por tanto no vas a necesitar utilizar el ratón. Cuando estamos programando, perdemos demasiado tiempo cambiando la mano del teclado al ratón y viceversa, pero con VIM, nos acostumbramos a no usarlo, disminuyendo así el tiempo que perdido.

La principal ventaja de VIM, es la productividad. Este editor de texto cuenta con 3 modos, a los que accedemos pulsando una sola tecla de nuestro teclado:

- **Modo normal:** Al pulsar la tecla escape (por defecto) accedemos a este modo, en el cual, las teclas de nuestro teclado cambiaran su funcionalidad, es decir, ahora al pulsar sobre las letras ejecutaremos un comando directamente.
- **Modo insertar:** Se accede al pulsar la tecla 'I' cuando estamos en modo normal. En este modo es donde escribiremos de forma natural nuestro código.
- **Modo visual:** Accedemos pulsando la letra 'V'. Este modo lo usaremos para seleccionar, de forma visual, el texto.

Estando en modo normal, tenemos a nuestra disposición toda una serie de comandos para movernos por el código, editar, copiar, borrar y un largo etc. Estos comandos los podemos combinar entre sí, de modo que, con práctica, seremos capaces de aumentar nuestra productividad abismalmente.

A todas estas ventajas hay que sumarle la posibilidad de añadir plugins creados por la comunidad, con lo cual podemos aumentar sus funcionalidades

**¿Y qué hay de las deventajas?**

Este editor no tiene interfaz gráfica, de modo que puede ser un caos. Otra desventaja es que todos los comandos vistos anteriormente te los tienes que aprender de memoria si realmente los quieres sacar provecho. VIM tiene una curba de dificultad muy alta de tal forma que si es la primera vez que lo utilizas, te va a costar mucho aprender como funciona todo.

Atajos y comandos básicos de VIM
Recuerdo que para poder utilizar estos comandos, debemos estar en modo normal.

| **Letra**      | **Uso**                                                                                                                                     |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| **h, j ,k, l** | Para desplazarnos, en lugar de movernos con las flechas del teclado, nos movemos con estas letras ( h: ← j: ↓ k: ↑ l: → )                   |
| **w, b**       | La w para desplazar el cursor una palabra hacia adelante y la b para desplazarnos una palabra hacia atrás                                   |
| **0, $**       | El 0 sirve para desplazarnos hacia el inicio de la linea en la que nos encontremos y $ para movernos al final                               |
| **^**          | Para movernos hasta el primer carácter no vacío de la linea                                                                                 |
| **gg, G**      | gg para movernos al principio del documento y G para movernos al final                                                                      |
| **d**          | Este comando se utiliza después de usar uno de los anteriores, elimina desde la posicion de nuestro cursor hasta el desplazamiento indicado |
| **dd**         | Elimina toda la linea sobre la que se encuentre el cursor                                                                                   |
| **y,p**        | y para copiar el texto hasta el desplazamiento que le indiquemos con los comandos anteriores, p para pegar                                  |
| **. (punto)**  | Con este comando repetiremos el comando ejecutado anteriormente                                                                             |

Hay muchisímos más comandos, pero he querido poner algunos de los principales, te animo a que sigas descubriendo nuevos comandos. Decir que los comandos anteriores se pueden combinar con números, es decir, si pulsamos sobre el 7 y luego sobre la w, nos moveremos 7 lineas hacia adelante. Aparte de estos atajos, hay que sumar, otras instrucciones que podemos escribimos:

**:q** Cierra vim.

**:w** Guardamos los cambios.

**:e** + “Nombre de la ruta” Para abrir el archivo que le indiquemos con la ruta.

**:split** Parte la pantalla verticalmente o horizontalmente para editar varios archivos a la vez.

**:Ex** Explorador de archivos.

**:help** Para ver el archivo de ayuda de Vim con todos los comandos.

## Cómo editar el archivo de configuración de vim

A VIM le podemos cambiar muchos de sus parámetros y opciones por defecto, para ello, teniendo VIM abierto, ejecutamos el comando:

`:e ~/.vimrc`

A continuación dejo algunas de las configuraciones que tengo yo puestas en mi archivo:

```
    " Vim por defecto crea archivos de backup pero son muy molestos con estas opciones los podemos quitar

    set nobackup
    set nowritebackup
    set noswapfile

    " Para hacer que VIM tabule automaticamente
    set autoindent

    "Para hacer que VIM utilize por defecto 2 espacios para lenguajes que se ven mejor así
    autocmd FileType html,css,sass,scss,javascript setlocal sw=2 sts=2
    autocmd FileType json setlocal sw=2 sts=2
    autocmd FileType ruby,eruby setlocal sw=2 sts=2
    autocmd FileType yaml setlocal sw=2 sts=2

    "Para que muestre a la izquierda los números de linea relativos, muy útil para cuando introducimos comandos
    set relativenumber

    "Para que resalte los paréntesis y los corchetes
    set showmatch
```

Como siempre, hay muchas más configuraciones, dependiendo de los gustos de cada uno.

---

<> with ❤️ by **Spectrasonic**
